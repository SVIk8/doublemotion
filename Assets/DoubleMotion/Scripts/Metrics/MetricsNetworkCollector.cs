﻿using UnityEngine;
using UnityEngine.Networking;

public class MetricsNetworkCollector : NetworkBehaviour {
	public float[][][] metricValues;
	public bool[][] metricAcception;
	private IMetric[][] metrics;
	private float[] networkValues;
	private bool[] networkAcceptions;

	public static int GESTURE_COUNT = 3;

	private void Start() {
		networkValues = new float[4 * GESTURE_COUNT * 2];
		networkAcceptions = new bool[4 * GESTURE_COUNT];
		GameObject metricsRoot = SOC.GetObject(4);

		metricValues = new float[4][][];
		metricAcception = new bool[4][];
		for (int i = 0; i < 4; ++i) {
			metricValues[i] = new float[GESTURE_COUNT][];
			metricAcception[i] = new bool[GESTURE_COUNT];
		}
		
		for (int i = 0; i < GESTURE_COUNT; ++i)
			metricValues[0][i] = new float[2];
		for (int i = 0; i < GESTURE_COUNT; ++i)
			metricValues[1][i] = new float[2];
		for (int i = 0; i < GESTURE_COUNT; ++i)
			metricValues[2][i] = new float[2];
		for (int i = 0; i < GESTURE_COUNT; ++i)
			metricValues[3][i] = new float[2];

		GlobalDecider.AddSource(this);

		if (!isLocalPlayer) return;
		metrics = new IMetric[4][];
		metrics[0] = metricsRoot.GetComponentsInChildren<Metric05>();
		metrics[1] = metricsRoot.GetComponentsInChildren<Metric1>();
		metrics[2] = metricsRoot.GetComponentsInChildren<Metric2>();
		metrics[3] = metricsRoot.GetComponentsInChildren<MetricInf>();

		TransformsHolder th = GetComponentInChildren<TransformsHolder>();
		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < GESTURE_COUNT; ++j)
				metrics[i][j].SetFrom(th);
	}

	private void Update() {
		if (!isLocalPlayer) return;

		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < GESTURE_COUNT; ++j) {
				metrics[i][j].GetTheDist(
					out networkValues[(i * GESTURE_COUNT + j) * 2],
					out networkValues[(i * GESTURE_COUNT + j) * 2 + 1]);
				networkAcceptions[i * GESTURE_COUNT + j] = metrics[i][j].IsAccepted();
			}

		CmdSendData(networkValues, networkAcceptions);
	}

	[Command]
	private void CmdSendData(float[] _metricValues, bool[] _metricAcceptions) {
		RpcBroadcastData(_metricValues, _metricAcceptions);
	}

	[ClientRpc]
	private void RpcBroadcastData(float[] _metricValues, bool[] _metricAcceptions) {
		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < GESTURE_COUNT; ++j) {
				metricValues[i][j][0] = _metricValues[(i * GESTURE_COUNT + j) * 2];
				metricValues[i][j][1] = _metricValues[(i * GESTURE_COUNT + j) * 2 + 1];
				metricAcception[i][j] = _metricAcceptions[(i * GESTURE_COUNT + j)];
			}
	}
}
