﻿using UnityEngine;

public class MassTH : MonoBehaviour {
	public VirtualHand[] virtualHands;

	public int massLength;
	public THLoader reader;

	private void OnEnable() {
		InitMass();
		ReadMass();
	}

	private void InitMass() {
		virtualHands = new VirtualHand[massLength];
	}

	private void ReadMass() {
		reader.ReadMass(out virtualHands, massLength);
	}
}
