﻿using UnityEngine;
using UnityEngine.Events;

public class Metric2 : MonoBehaviour, IMetric {
	public TransformsHolder fromTr;
	public TransformsHolder toTr;

	public float posDifScal;
	public float posEdge = .2f;
	private bool posCheck = false;
	public Animator posView;
	public UnityEvent onPosAccept;
	public UnityEvent onPosWrong;

	public float rotDifScal;
	public float rotEdge = 2000f;
	private bool rotCheck = false;
	public Animator rotView;
	public UnityEvent onRotAccept;
	public UnityEvent onRotWrong;

	private void Update() {
		GetDist(toTr.virtualHand, fromTr.virtualHand, out posDifScal, out rotDifScal);

		posView.SetFloat("State", Mathf.Clamp01(posDifScal / posEdge / 3));
		rotView.SetFloat("State", Mathf.Clamp01(rotDifScal / rotEdge / 3));

		if (posDifScal < posEdge && !posCheck) {
			posCheck = true;
			onPosAccept.Invoke();
		}

		if (posDifScal > posEdge && posCheck) {
			posCheck = false;
			onPosWrong.Invoke();
		}

		if (rotDifScal < rotEdge && !rotCheck) {
			rotCheck = true;
			onRotAccept.Invoke();
		}

		if (rotDifScal > rotEdge && rotCheck) {
			rotCheck = false;
			onRotWrong.Invoke();
		}
	}

	public void GetDist(VirtualHand _from, VirtualHand _to, out float _posDist, out float _rotDist) {
		if (_from == null || _to == null) { _posDist = _rotDist = -1f; return; }
		_posDist = _rotDist = 0f;

		for (int i = 1; i < _from.poss.Length; ++i) {
			_posDist += (_to.poss[i] - _from.poss[i]).magnitude 
						* (_to.poss[i] - _from.poss[i]).magnitude;
			_rotDist += Quaternion.Angle(_to.rots[i], _from.rots[i]) 
						* Quaternion.Angle(_to.rots[i], _from.rots[i]);
		}
		_posDist = Mathf.Sqrt(_posDist);
		_rotDist = Mathf.Sqrt(_rotDist);
	}

	public void GetTheDist(out float _posDist, out float _rotDist) {
		GetDist(toTr.virtualHand, fromTr.virtualHand, out _posDist, out _rotDist);
	}

	public bool IsAccepted() {
		return posCheck && rotCheck;
	}

	public void SetFrom(TransformsHolder _from) {
		toTr = _from;
	}

	public void SetTo(TransformsHolder _to) {
		fromTr = _to;
	}
}
