﻿using System.Collections.Generic;
using UnityEngine;

public class Decider : MonoBehaviour {
	public GameObject[] metricsHolders;
	private IMetric[] metrics;

	private int metricsCount;
	private List<int> accepted = new List<int>();
	private float
		closestPos, closestRot,
		lastPosDif, lastRotDif;
	private int bestId;

	public GameObject[] indicators;
	public GameObject noIndecator;
	private int lastIndicator = -2;

	private void Start() {
		metricsCount = metricsHolders.Length;
		metrics = new IMetric[metricsCount];
		for (int i = 0; i < metricsCount; ++i)
			metrics[i] = metricsHolders[i].GetComponent<IMetric>();
	}

	private void Update() {
		MakeDecision();
	}

	public void MakeDecision() {
		accepted.Clear();
		for (int i = 0; i < metricsCount; ++i)
			if (metrics[i].IsAccepted())
				accepted.Add(i);

		if (accepted.Count == 0) {
			if (lastIndicator == -1)
				return;
			if (lastIndicator != -2)
				indicators[lastIndicator].SetActive(false);
			lastIndicator = -1;
			noIndecator.SetActive(true);
		} else {
			closestPos = closestRot = float.MaxValue;
			for (int i = 0; i < accepted.Count; ++i) {
				metrics[accepted[i]].GetTheDist(out lastPosDif, out lastRotDif);
				if (lastPosDif < closestPos) {
					bestId = accepted[i];
					closestPos = lastPosDif;
					closestRot = lastRotDif;
				}
			}
			if (lastIndicator == bestId)
				return;
			if (lastIndicator == -1 || lastIndicator == -2)
				noIndecator.SetActive(false);
			else
				indicators[lastIndicator].SetActive(false);
			lastIndicator = bestId;
			indicators[bestId].SetActive(true);
		}
	}
}
