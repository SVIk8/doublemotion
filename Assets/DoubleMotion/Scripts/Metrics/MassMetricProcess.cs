﻿using UnityEngine;

public class MassMetricProcess : MonoBehaviour {
	public GameObject metricHolder;
	private IMetric metric;
	public TransformsHolder referenceTH;
	private VirtualHand reference;
	public MassTH targets;
	public float[] posDists;
	public float[] rotDists;

	private void OnEnable() {
		ProcessMassComparation();
	}

	private void ProcessMassComparation() {
		metric = metricHolder.GetComponent<IMetric>();
		reference = referenceTH.virtualHand;
		posDists = new float[targets.massLength];
		rotDists = new float[targets.massLength];

		for (int i = 0; i < targets.massLength; ++i)
			metric.GetDist(reference, targets.virtualHands[i],
				out posDists[i], out rotDists[i]);
	}
}
