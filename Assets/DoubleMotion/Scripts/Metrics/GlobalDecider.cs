﻿using System.Collections.Generic;
using UnityEngine;

public class GlobalDecider : MonoBehaviour {
	private List<MetricsNetworkCollector> sources = new List<MetricsNetworkCollector>();
	public GameObject[] metric05Id;
	public GameObject[] metric1Id;
	public GameObject[] metric2Id;
	public GameObject[] metricInfId;

	public float[] posEdge;
	public float[] rotEdge;

	public float[] metric05PosAvg = new float[MetricsNetworkCollector.GESTURE_COUNT];
	public float[] metric1PosAvg = new float[MetricsNetworkCollector.GESTURE_COUNT];
	public float[] metric2PosAvg = new float[MetricsNetworkCollector.GESTURE_COUNT];
	public float[] metricInfPosAvg = new float[MetricsNetworkCollector.GESTURE_COUNT];
	public float[] metric05RotAvg = new float[MetricsNetworkCollector.GESTURE_COUNT];
	public float[] metric1RotAvg = new float[MetricsNetworkCollector.GESTURE_COUNT];
	public float[] metric2RotAvg = new float[MetricsNetworkCollector.GESTURE_COUNT];
	public float[] metricInfRotAvg = new float[MetricsNetworkCollector.GESTURE_COUNT];

	private static GlobalDecider inst;

	private int[] lastId = new int[4];

	private void Start() {
		if (inst != null) {
			Debug.LogError("More then one GlobalDecider");
			return;
		}
		inst = this;
	}

	public static void AddSource(MetricsNetworkCollector _newSource) {
		inst.sources.Add(_newSource);
	}

	private void Update() {
		Avarage();
		Decide();
	}

	private void Avarage() {
		for (int i = 0; i < MetricsNetworkCollector.GESTURE_COUNT; ++i) {
			metric05PosAvg[i] = metric1PosAvg[i] = metric2PosAvg[i] = metricInfPosAvg[i] = 
				metric05RotAvg[i] = metric1RotAvg[i] = metric2RotAvg[i] = metricInfRotAvg[i] = 0;

			for (int j = 0; j < sources.Count; ++j) {
				metric05PosAvg[i] += Mathf.Sqrt(sources[j].metricValues[0][i][0]);
				metric1PosAvg[i] += Mathf.Sqrt(sources[j].metricValues[1][i][0]);
				metric2PosAvg[i] += Mathf.Sqrt(sources[j].metricValues[2][i][0]);
				metricInfPosAvg[i] += Mathf.Sqrt(sources[j].metricValues[3][i][0]);
				metric05RotAvg[i] += Mathf.Sqrt(sources[j].metricValues[0][i][1]);
				metric1RotAvg[i] += Mathf.Sqrt(sources[j].metricValues[1][i][1]);
				metric2RotAvg[i] += Mathf.Sqrt(sources[j].metricValues[2][i][1]);
				metricInfRotAvg[i] += Mathf.Sqrt(sources[j].metricValues[3][i][1]);
			}

			metric05PosAvg[i] /= sources.Count;
			metric1PosAvg[i] /= sources.Count;
			metric2PosAvg[i] /= sources.Count;
			metricInfPosAvg[i] /= sources.Count;
			metric05RotAvg[i] /= sources.Count;
			metric1RotAvg[i] /= sources.Count;
			metric2RotAvg[i] /= sources.Count;
			metricInfRotAvg[i] /= sources.Count;


			metric05PosAvg[i] *= metric05PosAvg[i];
			metric1PosAvg[i] *= metric1PosAvg[i];
			metric2PosAvg[i] *= metric2PosAvg[i];
			metricInfPosAvg[i] *= metricInfPosAvg[i];
			metric05RotAvg[i] *= metric05RotAvg[i];
			metric1RotAvg[i] *= metric1RotAvg[i];
			metric2RotAvg[i] *= metric2RotAvg[i];
			metricInfRotAvg[i] *= metricInfRotAvg[i];
		}
	}

	private void Decide() {
		float minPosAvg = float.MaxValue;
		int minId = -1;
		for (int i = 0; i < MetricsNetworkCollector.GESTURE_COUNT; ++i)
			if (metric05PosAvg[i] < posEdge[0] && metric05RotAvg[i] < rotEdge[0])
				if (metric05PosAvg[i] < minPosAvg) {
					minPosAvg = metric05PosAvg[i];
					minId = i;
				}
		if (lastId[0] != minId + 1)
			Decide05To(minId + 1);

		minPosAvg = float.MaxValue;
		minId = -1;
		for (int i = 0; i < MetricsNetworkCollector.GESTURE_COUNT; ++i)
			if (metric1PosAvg[i] < posEdge[1] && metric1RotAvg[i] < rotEdge[1])
				if (metric1PosAvg[i] < minPosAvg) {
					minPosAvg = metric1PosAvg[i];
					minId = i;
				}
		if (lastId[1] != minId + 1)
			Decide1To(minId + 1);

		minPosAvg = float.MaxValue;
		minId = -1;
		for (int i = 0; i < MetricsNetworkCollector.GESTURE_COUNT; ++i)
			if (metric2PosAvg[i] < posEdge[2] && metric2RotAvg[i] < rotEdge[2])
				if (metric2PosAvg[i] < minPosAvg) {
					minPosAvg = metric2PosAvg[i];
					minId = i;
				}
		if (lastId[2] != minId + 1)
			Decide2To(minId + 1);

		minPosAvg = float.MaxValue;
		minId = -1;
		for (int i = 0; i < MetricsNetworkCollector.GESTURE_COUNT; ++i)
			if (metricInfPosAvg[i] < posEdge[3] && metricInfRotAvg[i] < rotEdge[3])
				if (metricInfPosAvg[i] < minPosAvg) {
					minPosAvg = metricInfPosAvg[i];
					minId = i;
				}
		if (lastId[3] != minId + 1)
			DecideInfTo(minId + 1);
	}

	private void Decide05To(int _id) {
		metric05Id[lastId[0]].SetActive(false);
		lastId[0] = _id;
		metric05Id[_id].SetActive(true);
	}
	private void Decide1To(int _id) {
		metric1Id[lastId[1]].SetActive(false);
		lastId[1] = _id;
		metric1Id[_id].SetActive(true);
	}
	private void Decide2To(int _id) {
		metric2Id[lastId[2]].SetActive(false);
		lastId[2] = _id;
		metric2Id[_id].SetActive(true);
	}
	private void DecideInfTo(int _id) {
		metricInfId[lastId[3]].SetActive(false);
		lastId[3] = _id;
		metricInfId[_id].SetActive(true);
	}
}
