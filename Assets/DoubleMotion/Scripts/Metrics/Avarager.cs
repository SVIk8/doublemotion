﻿using UnityEngine;

public class Avarager : MonoBehaviour {
	public MassTH mass;
	public TransformsHolder target;

	private VirtualHand avaraged;

	private void OnEnable() {
		Avarage();
	}

	private void Avarage() {
		int massCount = mass.virtualHands.Length;
		avaraged = new VirtualHand();
		for (int i = 0; i < massCount; ++i)
			for (int j = 0; j < 16; ++j) {
				avaraged.posMagnitude = Mathf.Lerp(mass.virtualHands[i].posMagnitude, avaraged.posMagnitude, i / (i + 1));
				avaraged.poss[j] = Vector3.Lerp(mass.virtualHands[i].poss[j], avaraged.poss[j], i / (i + 1));
				avaraged.rots[j] = Quaternion.Slerp(mass.virtualHands[i].rots[j], avaraged.rots[j], i / (i + 1));
			}

		target.SetupNewData(avaraged);
	}
}
