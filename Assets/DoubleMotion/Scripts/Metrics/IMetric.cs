﻿public interface IMetric {
	void GetDist(VirtualHand _from, VirtualHand _to, out float _posDist, out float _rotDist);
	void GetTheDist(out float _posDist, out float _rotDist);
	void SetFrom(TransformsHolder _from);
	void SetTo(TransformsHolder _to);
	bool IsAccepted();
}
