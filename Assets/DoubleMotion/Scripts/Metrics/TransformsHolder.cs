﻿using System.IO;
using UnityEngine;

public class TransformsHolder : MonoBehaviour {
	public Transform[] handParts;
	public VirtualHand virtualHand;

	private float posMagnitude = 0f;

	private void Start() {
		virtualHand = new VirtualHand();
	}

	private void Update() {
		posMagnitude = 0f;
		virtualHand.poss[0] = handParts[0].position;
		virtualHand.rots[0] = handParts[0].rotation;
		for (int i = 1; i < handParts.Length; ++i) {
			virtualHand.poss[i] = handParts[0].InverseTransformPoint(handParts[i].position);
			posMagnitude += virtualHand.poss[i].magnitude;
			virtualHand.rots[i] = Quaternion.Inverse(virtualHand.rots[0]) * handParts[i].rotation;
		}
		posMagnitude /= handParts.Length - 1;
		virtualHand.posMagnitude = posMagnitude;
		for (int i = 1; i < handParts.Length; ++i)
			virtualHand.poss[i] /= posMagnitude;
	}

	public void SetupNewData(StreamReader _file) {
		Vector3 pos;
		Quaternion rot;
		for (int i = 0; i < 16; ++i) {
			pos.x = System.Convert.ToSingle(_file.ReadLine());
			pos.y = System.Convert.ToSingle(_file.ReadLine());
			pos.z = System.Convert.ToSingle(_file.ReadLine());
			rot.x = System.Convert.ToSingle(_file.ReadLine());
			rot.y = System.Convert.ToSingle(_file.ReadLine());
			rot.z = System.Convert.ToSingle(_file.ReadLine());
			rot.w = System.Convert.ToSingle(_file.ReadLine());
			handParts[i].position = pos;
			handParts[i].rotation = rot;
		}
	}

	public void SetupNewData(VirtualHand _vh) {
		posMagnitude = _vh.posMagnitude;
		for (int i = 1; i < 16; ++i)
			_vh.poss[i] *= posMagnitude;

		handParts[0].position = _vh.poss[0];
		handParts[0].rotation = _vh.rots[0];

		for (int i = 1; i < handParts.Length; ++i) {
			handParts[i].position = handParts[0].TransformPoint(_vh.poss[i]);
			handParts[i].rotation = handParts[0].rotation * _vh.rots[i];
		}
	}
}
