﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AproxConnector : MonoBehaviour {
	public CopySkeleton source;
	public int aproxSOCId;

	private void Start() {
		SOC.GetObject(aproxSOCId).GetComponent<Aproxer>().AddSource(source.to);
	}
}
