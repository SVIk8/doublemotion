﻿using System.Collections.Generic;
using UnityEngine;

public class Aproxer : MonoBehaviour {
	public Transform[] finalResault;
	private List<Transform[]> sources = new List<Transform[]>();

	public enum AproxType {
		m05,
		m1,
		m2,
		mInf
	}

	public AproxType at;

	public void AddSource(Transform[] _source) {
		sources.Add(_source);
	}

	private void Update() {
		if (sources.Count == 0)
			return;

		foreach (Transform t in finalResault) {
			t.position = Vector3.zero;
			t.rotation = Quaternion.identity;
		}
		
		switch (at) {
			case AproxType.m1:
				for (int i = 0; i < sources.Count; ++i) {
					for (int j = 0; j < sources[i].Length; ++j) {
						finalResault[j].localPosition = Vector3.Lerp(sources[i][j].localPosition, finalResault[j].localPosition, i / (float)sources.Count);
						finalResault[j].localRotation = Quaternion.Slerp(sources[i][j].localRotation, finalResault[j].localRotation, i / (float)sources.Count);
					}
				}
				break;
		}
		//switch (at) {
		//	case AproxType.m05:
		//		for (int i = 0; i < sources.Count; ++i) {
		//			for (int j = 0; j < sources[i].Length; ++j) {
		//				Quaternion.
		//			}
		//		}
		//		break;
		//	case AproxType.m1:
		//		break;
		//	case AproxType.m2:
		//		for (int i = 0; i < sources.Count; ++i) {
		//			for (int j = 0; j < sources[i].Length; ++j) {
		//				finalResault[j].localPosition = Vector3.Lerp(sources[i][j].localPosition, finalResault[j].localPosition, i / (float)sources.Count);
		//				finalResault[j].localRotation = Quaternion.Lerp(sources[i][j].localRotation, finalResault[j].localRotation, i / (float)sources.Count);
		//			}
		//		}
		//		break;
		//	case AproxType.mInf:
		//		for (int i = 0; i < sources.Count; ++i) {
		//			for (int j = 0; j < sources[i].Length; ++j) {
		//				finalResault[j].localPosition = Vector3.Lerp(sources[i][j].localPosition, finalResault[j].localPosition, i / (float)sources.Count);
		//				finalResault[j].localRotation = Quaternion.Lerp(sources[i][j].localRotation, finalResault[j].localRotation, i / (float)sources.Count);
		//			}
		//		}
		//		break;
		//}

		//for (int i = 0; i < sources.Count; ++i) {
		//	for (int j = 0; j < sources[i].Length; ++j) {
		//		finalResault[j].localPosition = Vector3.Lerp(sources[i][j].localPosition, finalResault[j].localPosition, i / (float)sources.Count);
		//		finalResault[j].localRotation = Quaternion.Lerp(sources[i][j].localRotation, finalResault[j].localRotation, i / (float)sources.Count);
		//	}
		//}
	}
}
