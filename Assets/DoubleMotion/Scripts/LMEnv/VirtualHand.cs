﻿using System.IO;
using UnityEngine;

public class VirtualHand {
	public Vector3[] poss;
	public Quaternion[] rots;
	public float posMagnitude = 0f;

	public VirtualHand(Quaternion[] _rots = null, Vector3[] _poss = null) {
		if (_rots != null)
			rots = (Quaternion[])_rots.Clone();
		else {
			rots = new Quaternion[16];
			for (int i = 0; i < 16; ++i)
				rots[i] = Quaternion.identity;
		}
		if (_poss != null)
			poss = (Vector3[])_poss.Clone();
		else {
			poss = new Vector3[16];
			for (int i = 0; i < 16; ++i)
				poss[i] = Vector3.zero;
		}
	}

	public VirtualHand(StreamReader _file) {
		rots = new Quaternion[16];
		poss = new Vector3[16];
		for (int i = 0; i < 16; ++i) {
			poss[i].x = System.Convert.ToSingle(_file.ReadLine());
			poss[i].y = System.Convert.ToSingle(_file.ReadLine());
			poss[i].z = System.Convert.ToSingle(_file.ReadLine());
			rots[i].x = System.Convert.ToSingle(_file.ReadLine());
			rots[i].y = System.Convert.ToSingle(_file.ReadLine());
			rots[i].z = System.Convert.ToSingle(_file.ReadLine());
			rots[i].w = System.Convert.ToSingle(_file.ReadLine());
		}

		Transform supTransform = SOC.GetObject(2).transform;
		supTransform.position = poss[0];
		supTransform.rotation = rots[0];
		for (int i = 1; i < 16; ++i) {
			poss[i] = supTransform.InverseTransformPoint(poss[i]);
			posMagnitude += poss[i].magnitude;
			rots[i] = Quaternion.Inverse(rots[0]) * rots[i];
		}
		posMagnitude /= 15;
		for (int i = 1; i < 16; ++i)
			poss[i] /= posMagnitude;
	}

	public VirtualHand(VirtualHand _other) {
		poss = (Vector3[])_other.poss.Clone();
		rots = (Quaternion[])_other.rots.Clone();
		posMagnitude = _other.posMagnitude;
	}
}
