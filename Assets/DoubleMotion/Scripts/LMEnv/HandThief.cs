﻿using System.Collections.Generic;
using UnityEngine;
using Leap;

public class HandThief : MonoBehaviour {
	public float refreshDelay = 3f;

	private Dictionary<int, Hand> hands = new Dictionary<int, Hand>();
	private Dictionary<int, float> times = new Dictionary<int, float>();

	private Hand tempHand = null;

	public void GrabTheHand(int _id, Hand _hand) {
		if (!hands.TryGetValue(_id, out tempHand)) {
			hands.Add(_id, _hand);
			times.Add(_id, Time.time);
		} else {
			hands[_id] = _hand;
			times[_id] = Time.time;
		}
	}
}
