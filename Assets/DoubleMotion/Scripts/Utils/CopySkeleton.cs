﻿using UnityEngine;
using UnityEngine.Networking;

public class CopySkeleton : MonoBehaviour {
	public int targetSOCId;
	public bool isCopying;
	public string dontSyncLabel = "DontSync";

	public Transform[] from;
	public Transform[] to;

	private void Start() {
		GameObject go = SOC.GetObject(targetSOCId);
		from = go.GetComponentsInChildren<Transform>();

		int i = 0;
		to = new Transform[from.Length];
		Transform[] transformPool = GetComponentsInChildren<Transform>();
		foreach(Transform t in transformPool) {
			Label label = t.GetComponent<Label>();
			if (label == null || !label.Check(dontSyncLabel))
				to[i++] = t;
		}

		NetworkIdentity ni = GetComponentInParent<NetworkIdentity>();
		if (ni != null && !ni.isLocalPlayer)
			isCopying = false;
	}

	private void Update () {
		if (!isCopying)
			return;

		for (int i = 0; i < to.Length; ++i) {
			to[i].position = from[i].position;
			to[i].rotation = from[i].rotation;
		}
	}
}
