﻿using UnityEngine;
using System.IO;

public class THSaver : MonoBehaviour {
	public string filePath;
	public string fileName;
	public int curId;

	public KeyCode recKey;
	public float dTime = 0.1f;
	private float lastRec = 0f;

	public TransformsHolder target;

	private void Update() {
		if (!Input.GetKey(recKey)) return;
		if (lastRec + dTime > Time.time) return;
		SaveNow();
		lastRec = Time.time;
	}

	public void SaveNow() {
		StreamWriter file = new StreamWriter(filePath + fileName + " (" + curId++.ToString() + ").txt");

		foreach (Transform tr in target.handParts) {
			file.WriteLine(tr.position.x);
			file.WriteLine(tr.position.y);
			file.WriteLine(tr.position.z);
			file.WriteLine(tr.rotation.x);
			file.WriteLine(tr.rotation.y);
			file.WriteLine(tr.rotation.z);
			file.WriteLine(tr.rotation.w);
		}

		file.Close();
	}

	private void OnGUI() {
		if (GUI.Button(new Rect(0f, 0f, 50f, 20f), "Save"))
			SaveNow();
		GUI.Label(new Rect(55f, 0f, 50f, 20f), curId.ToString());
	}
}
