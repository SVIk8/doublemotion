﻿using UnityEngine;
using System.IO;

public class THLoader : MonoBehaviour {
	public string filePath;
	public string fileName;
	public int curId;

	public KeyCode loadKey;
	public TransformsHolder target;

	private VirtualHand virtualHand;

	public void LoadTarget() {
		StreamReader file = new StreamReader(filePath + fileName + " (" + curId++.ToString() + ").txt");
		virtualHand = new VirtualHand(file);
		target.SetupNewData(virtualHand);
		file.Close();
	}

	public void ReadMass(out VirtualHand[] _virtualHands, int _numberOfFrames) {
		_virtualHands = new VirtualHand[_numberOfFrames];
		for (int i = 0; i < _numberOfFrames; ++i)
			ReadData(out _virtualHands[i], filePath + fileName + " (" + curId++.ToString() + ").txt");
	}

	private void ReadData(out VirtualHand _vh, string _file) {
		StreamReader file = new StreamReader(_file);
		_vh = new VirtualHand(file);
		file.Close();
	}

	private void Update() {
		if (Input.GetKeyDown(loadKey))
			LoadTarget();
	}

	private void OnGUI() {
		if (GUI.Button(new Rect(0f, 25f, 50f, 20f), "Load"))
			LoadTarget();
		GUI.Label(new Rect(55f, 25f, 50f, 20f), curId.ToString());
	}
}
