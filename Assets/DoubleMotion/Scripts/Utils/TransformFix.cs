﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TransformFix : NetworkBehaviour {
	public bool root = false;
	private bool prev = false;
	private bool isLocal = false;

	public Transform palmTr;

	private void Start() {
		NetworkIdentity ni = GetComponentInParent<NetworkIdentity>();
		isLocal = ni.isLocalPlayer;
	}

	private void OnGUI() {
		if (!isLocal) return;

		root = GUI.Toggle(new Rect(320, 5, 100, 20), root, "Root");
		if (root != prev) {
			SetRoot(root);
			prev = root;
		}
		if (GUI.Button(new Rect(100, 5, 100, 20), "Pos"))
			PosFix();
		if (GUI.Button(new Rect(210, 5, 100, 20), "Rot"))
			RotFix();
	}

	private void SetRoot(bool _state) {
		if (!_state) {
			CmdSetMeRoot(false);
			return;
		}

		TransformFix rootTf = FindRoot();
		if (rootTf == null)
			CmdSetMeRoot(true);
		else
			root = prev = false;
	}

	[Command]
	private void CmdSetMeRoot(bool _state) {
		if (!_state) {
			RpcSetMeRoot(false);
			return;
		}
		TransformFix rootTf = FindRoot();
		if (rootTf == null)
			RpcSetMeRoot(true);
		else
			RpcSetMeRoot(false);
	}

	[ClientRpc]
	private void RpcSetMeRoot(bool _state) {
		prev = root = _state;
	}

	private void RunFix() {
		if (root) return;
		TransformFix rootTf = FindRoot();

		if (rootTf == null)
			return;

		CopySkeleton cs = rootTf.GetComponentInChildren<CopySkeleton>();
		Transform rootPalm = cs.to[21];
		cs = GetComponentInChildren<CopySkeleton>();
		Transform localPalm = cs.to[21];
		Transform target = SOC.GetObject(3).transform;

		target.rotation = Quaternion.Inverse(Quaternion.FromToRotation(rootPalm.forward, localPalm.forward)) * target.rotation;
		target.position += rootPalm.position - localPalm.position;
	}

	private void PosFix() {
		if (root) return;
		TransformFix rootTf = FindRoot();

		if (rootTf == null)
			return;

		Transform target = SOC.GetObject(3).transform;

		target.position += rootTf.palmTr.position - palmTr.position;
	}

	private void RotFix() {
		if (root) return;
		TransformFix rootTf = FindRoot();

		if (rootTf == null)
			return;

		Transform target = SOC.GetObject(3).transform;

		target.rotation = Quaternion.Inverse(Quaternion.FromToRotation(rootTf.palmTr.forward, palmTr.forward)) * target.rotation;
	}

	private TransformFix FindRoot() {
		TransformFix rootTf = null;
		TransformFix[] tfs = FindObjectsOfType<TransformFix>();
		foreach (TransformFix tf in tfs)
			if (tf.root && tf != this) {
				rootTf = tf;
				break;
			}
		return rootTf;
	}
}
