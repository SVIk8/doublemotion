﻿using UnityEngine;

public class Label : MonoBehaviour {
	public string[] labels;

	public bool Check(string _label) {
		foreach(string label in labels)
			if (label == _label)
				return true;
		return false;
	}
}
